import React, { useEffect } from "react";
import axios from "axios";
import "./products.css";
import { API_URL } from "../../api";
import { useNavigate } from "react-router-dom";
import { useAppContext } from "../context/appContext";
const Products = () => {
  const { products, setProducts, deleteProduct, addToCart, product } =
    useAppContext();

  //useNavigate
  const navigate = useNavigate();

  // Adding request Interceptors
  axios.interceptors.request.use((config) => {
    // Print this before sending any HTTP request
    console.log(
      `${config.method.toUpperCase()} request sent to ${
        config.url
      } at ${new Date().getTime()}`
    );
    return config;
  });

  // adding response interceptors
  axios.interceptors.response.use((config) => {
    // Print this when receiving any HTTP response
    console.log("Response was received");
    return config;
  });

  //fetching products data from api
  useEffect(() => {
    try {
      const fetchProductData = async () => {
        const res = await axios.get(API_URL);

        setProducts(res.data.products);
        console.log(res.data.products);
      };
      fetchProductData();
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <>
      <div className="productsTitle">All Mobile Products</div>

      <div className="products">
        {products.map((product) => {
          const { productId, title, description, price, discountPercentage } =
            product;

          return (
            <div className="productsContainer" key={productId}>
              <div className="productWrapper">
                <div className="productDetails">
                  <li>
                    <span className="detailListItem">Model: </span>
                    {title}
                  </li>
                  <li>
                    <span className="detailListItem">Price: </span>${price}
                  </li>
                  <li>
                    <span className="detailListItem">Discount %: </span>
                    {discountPercentage}
                  </li>
                  <p>
                    <span className="detailListItem">Description: </span>
                    {description}
                  </p>
                </div>
                <div className="button">
                  <div
                    className="delete-btn"
                    onClick={() => deleteProduct(product.id)}
                  >
                    Delete
                  </div>
                  {/* To navigate to individual page */}

                  <div className="button">
                    <button
                      className="cart-btn"
                      onClick={() => addToCart(product)}
                    >
                      Cart
                    </button>
                  </div>
                  <div className="button">
                    <button
                      className="desc-btn"
                      onClick={() => navigate(`/products/${product.id}`)}
                    >
                      Descriptions
                    </button>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Products;
