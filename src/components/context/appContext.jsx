import { createContext, useState, useContext } from "react";
import axios from "axios";

//creating context
const appContext = createContext(null);

export const useAppContext = () => {
  const context = useContext(appContext);

  if (context === undefined) {
    throw new Error("Appcontext must be within appContextProvider!");
  }

  return context;
};

const AppContextProvider = (props) => {
  // for all products details
  const [products, setProducts] = useState([]);
  //for product descriptions
  const [product, setProduct] = useState({});
  // for product cart
  const [cart, setCart] = useState([]);

  // for product delete
  const deleteProduct = (id) => {
    axios.delete(`${id}`);
    setProducts(
      products.filter((product) => {
        return product.id !== id;
      })
    );
  };

  // for product add cart
  const addToCart = (product) => {
    const oldCart = [...cart];
    const newCart = oldCart.concat(product);
    setCart(newCart);
  };

  return (
    <appContext.Provider
      value={{
        products,
        setProducts,
        deleteProduct,
        product,
        setProduct,
        addToCart,
        cart,
        setCart,
      }}
    >
      {props.children}
    </appContext.Provider>
  );
};
export default AppContextProvider;
