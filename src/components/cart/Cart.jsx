import React from "react";
import "../product/product.css";
import "./cart.css"
import { useAppContext } from "./../context/appContext";
const Cart = () => {
  const { cart } = useAppContext();
  console.log("The Cart Products are", cart);

  return (
    <div className="productCart">
      {cart.map((product) => {
        const { title, description, price, discountPercentage } =
          product;

        return (
          <>
            <div className="product">
              <div className="productDetailWrapper">
                <div className="productDetails">
                  <li>
                    <span className="productdetailListItem">Model: </span>
                    {title}
                  </li>
                  <li>
                    <span className="productdetailListItem">Price: </span>{" "}
                    {price}
                  </li>
                  <li>
                    <span className="productdetailListItem">Discount %: </span>
                    {discountPercentage}
                  </li>
                  <p>
                    <span className="productdetailListItem">Description: </span>
                    {description}
                  </p>
                </div>
              </div>
            </div>
          </>
        );
      })}
    </div>
  );
};

export default Cart;
