import React from "react";
import "./navbar.css";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <div className="nav">
        <Link
          to="/"
          className="navLeftLogo"
          style={{ textDecoration: "none", color: "inherit" }}
        >
          <h1>Mobile Shop</h1>
        </Link>
        {/* <div className="navLeftLogo">
          <h1>Mobile Shop</h1>
        </div> */}
        <div className="navRight">
          <ul className="navRightList">
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
              <li className="navRightListItem">Home</li>
            </Link>

            <Link
              to="/cart"
              style={{ textDecoration: "none", color: "inherit" }}
            >
              <li className="navRightListItem">Cart</li>
            </Link>
          </ul>
        </div>
      </div>
    </>
  );
};

export default Navbar;
