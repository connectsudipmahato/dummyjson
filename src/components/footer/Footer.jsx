import React from "react";
import "./footer.css";
const Footer = () => {
  return (
    <div className="footer">
      <div className="footerDetails">
        <h2>All rights reserved by Sudip Kumar Mahato</h2>
        <p className="footerAddress">Kathmandu,Nepal</p>
      </div>
    </div>
  );
};

export default Footer;
