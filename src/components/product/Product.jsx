import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { PRODUCT_DETAILS_URL } from "../../api";
import { useAppContext } from "../context/appContext";
import "./product.css";
const Product = () => {
  const { product, setProduct } = useAppContext();
  // const [product, setProduct] = useState({});
  //fetching products data
  const params = useParams();
  const productId = params.productId;

  useEffect(() => {
    try {
      const fetchProduct = async () => {
        const res = await axios.get(`${PRODUCT_DETAILS_URL}/${productId}`);
        setProduct(res.data);
        console.log(res.data);
      };
      fetchProduct();
    } catch (err) {
      console.log(err);
    }
  }, [productId]);

  return (
    <>
      <span className="productTitle"> Product No. {productId}</span>
      <div className="product">
        <div className="productDetailWrapper">
          <div className="productDetails">
            <li>
              <span className="productdetailListItem">Model: </span>
              {product.title}
            </li>
            <li>
              <span className="productdetailListItem">Price: </span>{" "}
              {product.price}
            </li>
            <li>
              <span className="productdetailListItem">Discount %: </span>
              {product.discountPercentage}
            </li>
            <p>
              <span className="productdetailListItem">Description: </span>
              {product.description}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Product;
